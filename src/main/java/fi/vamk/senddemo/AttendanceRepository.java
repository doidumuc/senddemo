package fi.vamk.senddemo;

import java.util.Date;
import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);

    public Attendance findByDay(Date day);
}
