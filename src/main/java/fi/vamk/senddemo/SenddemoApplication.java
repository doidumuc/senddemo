package fi.vamk.senddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SenddemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SenddemoApplication.class, args);
	}

}
