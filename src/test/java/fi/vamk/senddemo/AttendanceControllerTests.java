package fi.vamk.senddemo;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;

import java.util.Date;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.senddemo" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceControllerTests {

    @Autowired
    private AttendanceRepository repository;

    @Test
    public void postGetDeleteAttendance() {

        Date now = new Date();

        Attendance att = new Attendance("ABCD", now);
        repository.save(att);
        Attendance found = repository.findByDay(att.getDay());
        System.out.println(found.toString());
        assertEquals(0, found.getDay().compareTo(att.getDay()));
        repository.delete(found);

    }
}
